import Vue from 'vue'
import Router from 'vue-router'

import Index from '../Index'
import Login from '../Login'
import IllustsDetail from '../view/IllustsDetail'
import Followed from "../view/Followed";
import UserDetail from "../view/UserDetail";
import ArtistDetail from "../view/ArtistDetail";

Vue.use(Router)

export default new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/index',
      name: 'Index',
      component: Index
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/illusts/:illustsId',
      name: 'IllustsDetail',
      component: IllustsDetail
    },
    {
      path: '/followed',
      name: 'Followed',
      component: Followed
    },
    {
      path: '/userDetail',
      name: 'UserDetail',
      component: UserDetail
    },
    {
      path: '/artist/:artistId',
      name: 'artist',
      component: ArtistDetail
    },
  ]
})
